function initMap() {
  var options = {
    center: {lat: 53.489028, lng: -2.25175},
    zoom: 18
  };

  map = new google.maps.Map(document.getElementById('map'), options);

  var marker = new google.maps.Marker({
    position: {lat: 53.489028, lng: -2.25175},
    map:map
  });

  var infoWindow = new google.maps.InfoWindow({
    content:  `<h4>Working Hours</h4>
                <ul>
                  <li>Monday: 10am - 4pm</li>
                  <li>Tuessday: 10am - 4pm</li>
                  <li>Wednesday: 10am - 4pm</li>
                  <li>Thursday: 10am - 4pm</li>
                  <li>Friday: 10am - 2pm</li>
                  <li>Saturday: Closed</li>
                  <li>Sunday: Closed</li>
                </ul>
              `
  });

  marker.addListener("click", function(){
    infoWindow.open(map, marker);
  });
}